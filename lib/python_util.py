import pandas as pd

def read_pickle_file(file):
    pickle_data = pd.read_pickle(file)
    return pickle_data

def read_parquet_file(file):
    parquet_data = pd.read_parquet(file)
    return parquet_data

def decode_census_data(file):
    df_raw = pd.read_feather(file)
    encoded_inc = df_raw['B19013_001']
    df_raw['B19013_001'] = encoded_inc.apply(lambda x: x.decode("utf-8"))
    df_clean = df_raw[['empi', 'B19013_001']]
    return df_clean
