#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 02_fit_lassos.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=25000]" bash 02_fit_lassos.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/02_fit_lassos.R >log/02_fit_lassos.log 2>&1
conda deactivate
