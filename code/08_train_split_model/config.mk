VPATH = temp
DIRS = log/.f temp/.f

SPLITS = temp/split_train_cohort.rds temp/split_val_cohort.rds

LASSOS = temp/lasso__stent_or_cabg_010_day__tested__1.rds \
temp/lasso__stent_or_cabg_010_day__tested__2.rds

TUNE_GBMS = temp/tuning__gbm__stent_or_cabg_010_day__tested__1.rds \
temp/tuning__gbm__stent_or_cabg_010_day__tested__2.rds

GBMS = temp/gbm__stent_or_cabg_010_day__tested__1.rds \
temp/gbm__stent_or_cabg_010_day__tested__2.rds

SUBSCORES = temp/subscores_val_set.rds temp/subscores_test_set.rds

ENSEMBLES = temp/ensemble__stent_or_cabg_010_day__tested__1.rds \
temp/ensemble__stent_or_cabg_010_day__tested__2.rds

SCORES = temp/scored_test_cohort.rds

CDF_PLOT = temp/test_rate_cdf.png

OUTPUT_FG_FNAMES = test_rate_cdf.png
OUTPUT_FGS = $(foreach fn,$(OUTPUT_FG_FNAMES),$(OUTPUT_DIR)/figures/$(fn))
