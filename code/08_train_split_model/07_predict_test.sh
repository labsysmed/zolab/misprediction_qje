#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_predict_test.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 07_predict_test.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/07_predict_test.R >log/07_predict_test.log 2>&1
conda deactivate
