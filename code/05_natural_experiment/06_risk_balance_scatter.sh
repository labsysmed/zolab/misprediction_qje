#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 06_risk_balance_scatter.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 06_risk_balance_scatter.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/06_risk_balance_scatter.R >log/06_risk_balance_scatter.log 2>&1
conda deactivate
