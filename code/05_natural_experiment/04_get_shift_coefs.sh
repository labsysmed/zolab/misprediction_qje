#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 04_get_shift_coefs.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 04_get_shift_coefs.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/04_get_shift_coefs.R >log/04_get_shift_coefs.log 2>&1
conda deactivate
