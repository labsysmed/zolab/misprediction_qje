#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 03_shift_random_effects.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=30000]" bash 03_shift_random_effects.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/03_shift_random_effects.R >log/03_shift_random_effects.log 2>&1
conda deactivate
