#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_leaveout_shift_test_rates.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 01_leaveout_shift_test_rates.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/01_leaveout_shift_test_rates.R >log/01_leaveout_shift_test_rates.log 2>&1
conda deactivate
