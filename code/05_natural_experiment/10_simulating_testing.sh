#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 10_simulating_testing.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 10_simulating_testing.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/10_simulating_testing.R >log/10_simulating_testing.log 2>&1
conda deactivate
