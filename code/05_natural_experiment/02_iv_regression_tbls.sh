#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 02_iv_regression_tbls.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 02_iv_regression_tbls.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/02_iv_regression_tbls.R >log/02_iv_regression_tbls.log 2>&1
conda deactivate
