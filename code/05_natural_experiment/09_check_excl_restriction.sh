#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 09_check_excl_restriction.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 09_check_excl_restriction.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/09_check_excl_restriction.R >log/09_check_excl_restriction.log 2>&1
conda deactivate
