VPATH = temp temp/plots
DIRS = log/.f temp/$(SPLIT)/plots/.f
COHORT = /data/zolab/stressed_ensemble/data/analysis/full_cohort.rds
OUTPUT_DIR = ../../../output

LEAVEOUT_TEST_RATES = temp/shift_test_rates.rds

IV_REG_TBLS = temp/longterm_regs__adverse__yhat.tex \
temp/longterm_regs__adverse_interacted__yhat.tex \
temp/yield_test__on__shift_trate__regs.tex

RE_MODELS = temp/re_models.rds
RE_BINS = temp/shift_bins.rds

MARGINAL_TESTS = temp/plots/test_010_day__by__tile_005__for__bin_re_shift_12_full_test_010_day_inclyhat_linear.png

BALANCE_SCATTER = temp/plots/scatter__yhatbar__by__leaveout_trate.png

NATEXP_STATS = log/07_natexp_stats.log

PREV_K_HRS = temp/tests_in_k_hours_df.rds
EXCL_RESTRICTION_REGS = temp/exclusion_restriction_regs.tex

SIM_TESTING = log/10_simulating_testing.log

OUTPUT_TBL_FNAMES = longterm_regs__adverse__yhat.tex \
longterm_regs__adverse_interacted__yhat.tex \
yield_test__on__shift_trate__regs.tex
OUTPUT_TBLS = $(foreach fn,$(OUTPUT_TBL_FNAMES),$(OUTPUT_DIR)/tables/$(fn))

OUTPUT_FG_FNAMES = scatter__yhatbar__by__leaveout_trate.png \
test_010_day__by__tile_005__for__bin_re_shift_12_full_test_010_day_inclyhat_linear.png \
test_010_day__by__tile_005_ensemble_ecg__for__bin_re_shift_12_full_test_010_day_inclyhat_linear.png
OUTPUT_FGS = $(foreach fn,$(OUTPUT_FG_FNAMES),$(OUTPUT_DIR)/figures/$(fn))
