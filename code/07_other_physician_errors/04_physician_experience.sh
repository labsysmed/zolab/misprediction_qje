#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 04_physician_experience.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 04_physician_experience.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/04_physician_experience.R >log/04_physician_experience.log 2>&1
conda deactivate
