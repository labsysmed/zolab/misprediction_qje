#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 02_symptom_salience_regs.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 02_symptom_salience_regs.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/02_symptom_salience_regs.R >log/02_symptom_salience_regs.log 2>&1
conda deactivate
