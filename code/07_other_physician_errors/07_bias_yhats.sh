#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_bias_yhats.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 07_bias_yhats.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/07_bias_yhats.R >log/07_bias_yhats.log 2>&1
conda deactivate
