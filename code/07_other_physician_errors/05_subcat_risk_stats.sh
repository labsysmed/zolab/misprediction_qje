#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_subcat_risk_stats.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 05_subcat_risk_stats.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/05_subcat_risk_stats.R >log/05_subcat_risk_stats.log 2>&1
conda deactivate
