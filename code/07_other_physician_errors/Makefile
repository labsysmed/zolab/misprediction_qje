# ------------------------------------------------------------------------------
# Makes all targets for 07_other_physician_errors directory
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=150000]" make
# ------------------------------------------------------------------------------

# Config -----------------------------------------------------------------------
.ONESHELL:
include config.mk

# Recipes ----------------------------------------------------------------------
## all			: Makes all physician error tables and figures
.PHONY: all
all: cc_salience_reg demographic_reg phys_experience_reg \
salience_reg output_fgs output_tbls # phys_risk_wt

# Intermediate recipes
$(COHORT): ../02_prep_and_summarize_cohort/
	$(MAKE) -C ../02_prep_and_summarize_cohort

$(REP_VARS): 01_get_representative_ccs.sh \
scripts/01_get_representative_ccs.R $(COHORT) $(DIRS)
	bash $<

$(CC_FREQUENCY): 01_get_representative_ccs.sh \
scripts/01_get_representative_ccs.R $(COHORT) $(DIRS)
	bash $<

# Rep vars aren't a pre-req in modeling script, so rebuild won't trigger
# new run of modeling pipeline. To make modeling pipeline re-run,
# which is only necessary if list of rep vars substantively changes, run
# rm -rf /data/zolab/stressed_ensemble/data/modeling/models/random/represent.

$(REP_YHATS): ../01_build_models/01_primary_models/scripts/ $(REP_VARS)
	$(MAKE) -C ../01_build_models/01_primary_models RESTRICTION=represent
	touch $@

$(CC_YHATS): ../01_build_models/01_primary_models/scripts/
		$(MAKE) -C ../01_build_models/01_primary_models RESTRICTION=justcc

# Final output
## cc_frequency_tb	: Table of frequency and representativeness for top 20 ccs
.PHONY: cc_frequency_tb
cc_frequency_tb: $(CC_FREQUENCY_TB)
$(CC_FREQUENCY_TB): 01_get_representative_ccs.sh \
scripts/01_get_representative_ccs.R $(COHORT) $(DIRS)
	bash $<

## cc_salience_reg	: Table for symptom salience/representativeness regs
.PHONY: cc_salience_reg
cc_salience_reg: $(CC_SALIENCE_REG)
$(CC_SALIENCE_REG): 02_symptom_salience_regs.sh \
scripts/02_symptom_salience_regs.R $(CC_FREQUENCY) $(REP_YHATS) \
$(CC_YHATS) $(DIRS)
	bash $<

## demographic_reg	: Table for demographic regs
.PHONY: demographic_reg
demographic_reg: $(DEMOGRAPHIC_REG)
$(DEMOGRAPHIC_REG): 03_demographic_risk_tbl.sh \
scripts/03_demographic_risk_tbl.R $(COHORT) $(DIRS)
	bash $<

## phys_experience_reg	: Table for physician experience regs
.PHONY: phys_experience_reg
phys_experience_reg: $(PHYS_EXPERIENCE_REG)
$(PHYS_EXPERIENCE_REG): 04_physician_experience.sh \
scripts/04_physician_experience.R $(COHORT) $(DIRS)
	bash $<

## subcat_stats		: Log file reporting stats on subcat risk weights
.PHONY: subcat_stats
subcat_stats: $(SUBCAT_STATS)
$(SUBCAT_STATS): 05_subcat_risk_stats.sh \
scripts/05_subcat_risk_stats.R $(COHORT) $(DIRS)
	bash $<

## salience_reg		: Table for feature subcategory salience regs
.PHONY: salience_reg
salience_reg: $(SALIENCE_REG)
$(SALIENCE_REG): 06_salience_regression_tbl.sh \
scripts/06_salience_regression_tbl.R $(COHORT) $(DIRS)
	bash $<

## bias_yhat_plots	: Bar plots of physician errors by bias levels
.PHONY: bias_yhat_plots
bias_yhat_plots: $(BIAS_YHAT_PLOTS)
$(BIAS_YHAT_PLOTS): 07_bias_yhats.sh scripts/07_bias_yhats.R \
$(COHORT) $(DIR)
	bash $<

## output_tbls		: Copies tables used in paper to `output` directory
.PHONY: output_tbls
output_tbls: $(OUTPUT_TBLS)
$(OUTPUT_DIR)/tables/%.tex : %.tex
	cp $< $@

## output_fgs		: Copies tables used in paper to `output` directory
.PHONY: output_fgs
output_fgs: $(OUTPUT_FGS)
$(OUTPUT_DIR)/figures/%.png : %.png
	cp $< $@

# Helper Recipes ---------------------------------------------------------------
.PHONY: dirs
dirs: $(DIRS)
%/.f :
	mkdir -p $(dir $@)
	touch $@

.PHONY: clean
clean:
	rm -rf temp log

## style			: Styles R scripts according to tidyverse guidelines
.PHONY: style
style:
	source ~/anaconda3/etc/profile.d/conda.sh
	conda activate stressr
	R --vanilla --silent -e "styler::style_dir(path = 'scripts', filetype = c('R', 'Rmd', 'Rprofile'))"

## help			: Prints available .PHONY targets with description
.PHONY : help
help : Makefile
	@sed -n 's/^##//p' $<
