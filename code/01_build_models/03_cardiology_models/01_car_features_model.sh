#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_car_features_model.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 05_car_features_model.sh {split}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $1 == overnight ]
then split=_overnight
else split=""
fi

Rscript --vanilla --verbose scripts/05_car_features_model.R --split $1 >log/05_car_features_model$split.log 2>&1
conda deactivate
