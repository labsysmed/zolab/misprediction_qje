#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_predict_ensemble.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 07_predict_ensemble.sh {split} {restriction}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $1 == TRUE ]
then split=_overnight
else split=""
fi

if [ $2 == "all" ]
then restriction=""
else restriction=_$2
fi

Rscript --vanilla --verbose scripts/07_predict_ensemble.R --split $1 --restriction $2 >log/07_predict_ensemble$split$restriction.log 2>&1
conda deactivate
