#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_examine_outcomes.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=10000]" bash 07_examine_outcomes.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/07_examine_outcomes.R >log/07_examine_outcomes.log 2>&1
conda deactivate
