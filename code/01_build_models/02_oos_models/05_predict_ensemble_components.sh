#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_predict_ensemble_components.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=25000]" bash 05_predict_ensemble_components.sh {split} {restriction}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $1 == "overnight" ]
then split=_overnight
else split=""
fi

if [ $2 == "all" ]
then restriction=""
else restriction=_$2
fi

Rscript --vanilla --verbose scripts/05_predict_ensemble_components.R --split $1 --restriction $2 >log/05_predict_ensemble_components$split$restriction.log 2>&1
conda deactivate
