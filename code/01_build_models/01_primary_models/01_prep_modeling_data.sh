#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_prep_modeling_data.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=60000]" bash 01_prep_modeling_data.sh {split}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $1 == "overnight" ]
then split=_overnight
else split=""
fi

Rscript --vanilla --verbose scripts/01_prep_modeling_data.R --split $1 >log/01_prep_modeling_data$split.log 2>&1
conda deactivate
