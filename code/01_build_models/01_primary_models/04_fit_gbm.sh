#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 04_fit_gbm.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=25000]" bash 04_fit_gbm.sh {outcome} {split} {restriction}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $2 == "overnight" ]
then split=_overnight
else split=""
fi

if [ $3 == "all" ]
then restriction=""
else restriction=_$3
fi

Rscript --vanilla --verbose scripts/04_fit_gbm.R --outcome $1 --split $2 --restriction $3 >log/04_fit_gbm_$1$split$restriction.log 2>&1
conda deactivate
