#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 03_tune_gbm.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=25000]" bash 03_tune_gbm.sh {outcome} {split} {restriction}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $2 == "overnight" ]
then split=_overnight
else split=""
fi

if [ $3 == "all" ]
then restriction=""
else restriction=_$3
fi

Rscript --vanilla --verbose scripts/03_tune_gbm.R --outcome $1 --split $2 --restriction $3 >log/03_tune_gbm_$1$split$restriction.log 2>&1
conda deactivate
