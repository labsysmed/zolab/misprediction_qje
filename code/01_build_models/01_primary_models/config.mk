MODELING=/data/zolab/stressed_ensemble/data/modeling

TUNING=$(MODELING)/tuning/$(SPLIT)/$(RESTRICTION)
MODELS=$(MODELING)/models/$(SPLIT)/$(RESTRICTION)
SUBSCORES=$(MODELING)/subscores/$(SPLIT)/$(RESTRICTION)
PREDICTION=$(MODELING)/prediction/$(SPLIT)/$(RESTRICTION)
COHORTS=$(MODELING)/cohorts/$(SPLIT)

MODELING_DIRS=$(MODELING) $(TUNING) $(MODELS) $(SUBSCORES) $(PREDICTION) $(COHORTS)

ANALYSIS=/data/zolab/stressed_ensemble/data/analysis

RESTRICTIONS=all dropcc justcc dem enc dia lab lvs med prc represent
SPLITS=random overnight

DOWNSAMPLES=ds_mace ds_test tested
COHORT_FNS=train_cohort.rds test_cohort.rds val_cohort.rds \
$(foreach ds,$(DOWNSAMPLES),train_cohort_$(ds).rds)
COHORT_FILES=$(foreach fn,$(COHORT_FNS),$(COHORTS)/$(fn))

OUTCOMES=stent_or_cabg_010_day__tested macetrop_030_pos__untested #death_030_day__untested #macetrop_pos_or_death_030__untested #test_010_day__all
