#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_predict_ensemble.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=25000]" bash 07_predict_ensemble.sh dataset
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $2 == "overnight" ]
then split=_overnight
else split=""
fi

if [ $3 == "all" ]
then restriction=""
else restriction=_$3
fi

Rscript --vanilla --verbose scripts/07_predict_ensemble.R --dataset $1 --split $2 --restriction $3 >log/07_predict_ensemble_$1$split$restriction.log 2>&1
conda deactivate
