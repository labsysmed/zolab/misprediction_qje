#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_testing_plots.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 07_testing_plots.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/07_adverse_event_tbl.R >log/07_adverse_event_tbl.log 2>&1
conda deactivate
