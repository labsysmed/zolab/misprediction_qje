# Score Diagnostics
This directory takes as inputs our cleaned up cohort and outcome files from `02_prep_and_summarize_cohort` and outputs plots and regression tables analyzing the relationships between predicted risk and various outcomes. These scripts build Figure 1a, Figure 2, Figure 3, Figures A.4-6, and Table A.10.
