#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 04_regress_longterm_adverse.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 04_regress_longterm_adverse.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/04_regress_longterm_adverse.R >log/04_regress_longterm_adverse.log 2>&1
conda deactivate
