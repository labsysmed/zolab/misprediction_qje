#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 08_report_outcome_stats.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 08_report_outcome_stats.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/08_report_outcome_stats.R >log/08_report_outcome_stats.log 2>&1
conda deactivate
