VPATH = temp/01_plot_yield temp/02_plot_adverse_outcomes \
temp/03_plot_longterm_adverse temp/04_regress_longterm_adverse \
temp/05_plot_stress_cath_calib temp/06_sameday_tn_plots \
temp/07_adverse_event_tbl temp/08_report_outcome_stats

TILE5 = tile_stent_or_cabg_005_tested
STENT = stent_or_cabg_010_day

COHORT = /data/zolab/stressed_ensemble/data/analysis/test_cohort.rds

TESTED_PLOTS = temp/01_plot_yield/test_010_day__by__tile_010.png \
temp/01_plot_yield/$(STENT)__by__tile_010.png

ADVERSE_PLOTS = temp/02_plot_adverse_outcomes/macetrop_pos_or_death_030__by__tile_010__for__full.png \
temp/02_plot_adverse_outcomes/untested_noecg__by__tile_004__for__full.png \
temp/02_plot_adverse_outcomes/death_030_day__by__tile_004__for__full.png \
temp/02_plot_adverse_outcomes/macetrop_030_pos__by__tile_004__for__full.png

LONGTERM_ADVERSE_PLOTS = temp/03_plot_longterm_adverse/death_365_day__by__tile_004__for__test_010_day.png

LONGTERM_ADVERSE_REGS = temp/04_regress_longterm_adverse/longterm_regs__adverse_interacted__yhat__test_010_day.tex

STRESS_CATH_PLOTS = temp/05_plot_stress_cath_calib/$(STENT)__by__tile_010__for__stress_010_day.png

SAMEDAY_TN_PLOTS = temp/06_sameday_tn_plots/sameday_tn__$(STENT).png

ADVERSE_TBL = temp/07_adverse_event_tbl/adverse_outcome_tbl.tex

OUTCOME_STATS = log/08_report_outcome_stats.log

OUTPUT_DIR = ../../../output

OUTPUT_TBL_FNAMES = adverse_outcome_tbl.tex
OUTPUT_TBLS = $(foreach fn,$(OUTPUT_TBL_FNAMES),$(OUTPUT_DIR)/tables/$(fn))

OUTPUT_FG_FNAMES = $(STENT)__by__tile_010.png \
untested_noecg__by__tile_004__for__full.png \
untested_not_sameday_tn__by__tile_004__for__full.png \
macetrop_pos_or_death_030__by__tile_010__for__full.png \
death_030_day__by__tile_004__for__full.png \
macetrop_030_pos__by__tile_004__for__full.png \
macetrop_pos_or_death_030__by__tile_004__for__noecg.png \
macetrop_pos_or_death_030__by__tile_004__for__not_sameday_tn.png \
macetrop_005_or_death_030__by__tile_010__for__full.png \
macetrop_01_or_death_030__by__tile_010__for__full.png \
macetrop_05_or_death_030__by__tile_010__for__full.png \
macetrop_pos_or_death_030__by__tile_004__for__not_admit_sym.png \
macetrop_030_pos__by__tile_004__for__not_admit_sym.png \
death_030_day__by__tile_004__for__not_admit_sym.png \
sameday_tn__ami_day_of.png sameday_tn__test_010_day.png \
sameday_tn__stent_or_cabg_010_day.png \
death_365_day__by__tile_004__for__test_010_day.png
OUTPUT_FGS = $(foreach fn,$(OUTPUT_FG_FNAMES),$(OUTPUT_DIR)/figures/$(fn))
