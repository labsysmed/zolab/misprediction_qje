#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 06_sameday_tn_plots.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 06_sameday_tn_plots.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/06_sameday_tn_plots.R >log/06_sameday_tn_plots.log 2>&1
conda deactivate
