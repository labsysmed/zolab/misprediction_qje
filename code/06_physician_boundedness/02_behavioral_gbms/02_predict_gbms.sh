#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 02_predict_gbms.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=50000]" bash 02_predict_gbms.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/02_predict_gbms.R >log/02_predict_gbms.log 2>&1
conda deactivate
