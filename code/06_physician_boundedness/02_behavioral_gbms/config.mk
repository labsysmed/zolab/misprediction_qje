DIRS = log/.f temp/.f
DATA_DIR = /data/zolab/stressed_ensemble/data

COHORT = $(DATA_DIR)/analysis/test_cohort.rds

GBMS = temp/gbms__stent_or_cabg_010_day__tested.rds

PERFORMANCE_ALL = temp/performance_list.rds

PERFORMANCE_SUM = temp/performance.rds

PERFORMANCE_PLOT = temp/BEHAVIORAL_GBM_AUC.png temp/BEHAVIORAL_GBM_R2.png
