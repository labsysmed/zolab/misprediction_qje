#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_fit_lasso.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=50000]" bash 01_fit_lasso.sh {outcome}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/01_fit_lasso.R --target $1 >log/01_fit_lasso_$(basename $1 .yml).log 2>&1
conda deactivate
