#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_measure_performance.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q medium -R "rusage[mem=1000]" bash 05_measure_performance.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/05_measure_performance.R >log/05_measure_performance.log 2>&1
conda deactivate
