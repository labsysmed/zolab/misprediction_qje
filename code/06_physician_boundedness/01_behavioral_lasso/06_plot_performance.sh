#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 06_plot_performance.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q medium -R "rusage[mem=1000]" bash 06_plot_performance.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/06_plot_performance.R >log/06_plot_performance.log 2>&1
conda deactivate
