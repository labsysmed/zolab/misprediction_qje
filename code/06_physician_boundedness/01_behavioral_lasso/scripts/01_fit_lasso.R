# ------------------------------------------------------------------------------
# Runs lasso for doctors' simplified risk model
# Updates author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=50000]" bash 01_fit_lasso.sh {outcome}
# ------------------------------------------------------------------------------

# Seeding ----------------------------------------------------------------------
set.seed(1)

# Libraries --------------------------------------------------------------------
library(here)
library(yaml)
library(optparse)
library(glue)
library(data.table)
library(Matrix)
library(glmnet)
library(dplyr)

temp <- here(
  "code", "06_physician_boundedness", "01_behavioral_lasso", "temp"
)

# Command Line -----------------------------------------------------------------
option_list <- list(
  make_option("--target", type = "character", help = "name of target variable")
)
args <- parse_args(OptionParser(option_list = option_list))


# Load Data --------------------------------------------------------------------
message("Loading data...")
paths <- read_yaml(here("lib", "filepaths.yml"))

sample <- case_when(
  args$target == "test_010_day" ~ "train_ds_test",
  args$target == "stent_or_cabg_010_day" ~ "train_tested"
)
ids_fn <- case_when(
  args$target == "test_010_day" ~ "train_cohort_ds_test.rds",
  args$target == "stent_or_cabg_010_day" ~ "train_cohort_tested.rds"
)
split <- "random"
x <- readRDS(glue(paths$features[[sample]]))
cohort_fp <- file.path(paths$modeling$dir, "cohorts", "random")
ids <- readRDS(file.path(cohort_fp, ids_fn))

keep <- which(!ids$excl_flag_c_int & !ids$excl_flag_chronic & !ids$excl_flag_death)

x <- x[keep, ]
ids <- ids[keep, ]

# Fit Model --------------------------------------------------------------------
message("Fitting model...")
model <- glmnet(
  x, ids[[args$target]], family = "binomial"#, lambda.min.ratio = 1e-08
)

# Save -------------------------------------------------------------------------
message("Saving model...")
saveRDS(model, file.path(temp, glue("lasso__{args$target}.rds")))

message("Done.")
