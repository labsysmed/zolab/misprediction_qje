#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 09_leftover_signal.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 09_leftover_signal.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/09_leftover_signal.R >log/09_leftover_signal.log 2>&1
conda deactivate
