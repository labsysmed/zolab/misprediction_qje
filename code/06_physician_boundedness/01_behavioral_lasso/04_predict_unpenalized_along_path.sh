#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 04_predict_unpenalized_along_path.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q medium -R "rusage[mem=1000]" bash 04_predict_unpenalized_along_path.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/04_predict_unpenalized_along_path.R >log/04_predict_unpenalized_along_path.log 2>&1
conda deactivate
