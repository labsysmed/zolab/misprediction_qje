#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 04_mace_vs_yield.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 04_mace_vs_yield
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/04_mace_vs_yield.R >log/04_mace_vs_yield.log 2>&1
conda deactivate
