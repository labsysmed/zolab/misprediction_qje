#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 10_calc_life_expectancy.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=10000]" bash 10_calc_life_expectancy.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/10_calc_life_expectancy.R >log/10_calc_life_expectancy.log 2>&1
conda deactivate
