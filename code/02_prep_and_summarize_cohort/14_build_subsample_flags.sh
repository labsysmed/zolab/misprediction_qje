#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 14_build_subsample_flags.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 14_build_subsample_flags.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/14_build_subsample_flags.R >log/14_build_subsample_flags.log 2>&1
conda deactivate
