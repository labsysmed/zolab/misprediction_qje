#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 03_build_trop_outcomes.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q small -R "rusage[mem=100]" bash 03_build_trop_outcomes.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/03_build_trop_outcomes.R >log/03_build_trop_outcomes.log 2>&1
conda deactivate
