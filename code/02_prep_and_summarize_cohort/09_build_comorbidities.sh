#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 09_build_comorbidities.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=10000]" bash 09_build_comorbidities.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/09_build_comorbidities.R >log/09_build_comorbidities.log 2>&1
conda deactivate
