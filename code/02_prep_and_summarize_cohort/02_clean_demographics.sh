#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 02_clean_demographics.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=10000]" bash 02_clean_demographics.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/02_clean_demographics.R >log/02_clean_demographics.log 2>&1
conda deactivate
