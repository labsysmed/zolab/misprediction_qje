#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 13_build_longterm_outcomes.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 13_build_longterm_outcomes.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/13_build_longterm_outcomes.R >log/13_build_longterm_outcomes.log 2>&1
conda deactivate
