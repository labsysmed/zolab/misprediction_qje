#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 12_disch_dx.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 12_disch_dx.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/12_disch_dx.R >log/12_disch_dx.log 2>&1
conda deactivate
