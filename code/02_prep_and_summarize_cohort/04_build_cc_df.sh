#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 04_build_cc_df.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 04_build_cc_df.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/04_build_cc_df.R >log/04_build_cc_df.log 2>&1
conda deactivate
