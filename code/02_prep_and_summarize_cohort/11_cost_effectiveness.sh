#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 11_cost_effectiveness.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 11_cost_effectiveness.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/11_cost_effectiveness.R >log/11_cost_effectiveness.log 2>&1
conda deactivate
