ANALYSIS = /data/zolab/stressed_ensemble/data/analysis
MODELING = /data/zolab/stressed_ensemble/data/modeling
DIRS = log/.f temp/.f $(ANALYSIS)/.f

RISK_FACTORS = $(ANALYSIS)/risk_factors.rds
DEMOGRAPHICS = $(ANALYSIS)/demographics.rds
TROPONIN = $(ANALYSIS)/max_troponin.rds
CHIEF_COMPLAINTS = $(ANALYSIS)/cc_full_cohort.rds

ifeq ($(SPLIT), overnight)
OVERNIGHT_LAB = _overnight
endif

RISK_PREDICTIONS = $(MODELING)/oos/prediction/random/all/scores_train_set.rds \
$(MODELING)/prediction/random/all/scores_test_set.rds \
$(MODELING)/prediction/random/all/scores_test_set_ECG.rds

AMI_ENCOUNTERS = /data/zolab/stressed_ensemble/data/cohort/ami_encounters.rds
COHORT = $(ANALYSIS)/full_cohort$(OVERNIGHT_LAB).rds \
$(ANALYSIS)/test_cohort$(OVERNIGHT_LAB).rds

COMMON_ZCS = temp/zc_counts.csv
DESCRIPTIVE_TB = temp/desc_stats_summary.tex temp/desc_stats_suspicion.tex
PRIMARY_DX = $(ANALYSIS)/primary_diagnoses.rds

COMORBIDITIES = temp/comorbidities_df.rds
LIFE_EXP = $(ANALYSIS)/life_expectancy.rds
COST_EFF = $(ANALYSIS)/daly_cost_effectiveness.rds

DISCH_DX = $(ANALYSIS)/disch_dx.rds
LONGTERM_OUTCOMES = $(ANALYSIS)/longterm_outcomes.rds

SUBSAMPLE_FLAGS = $(ANALYSIS)/subsample_flags.rds

OUTPUT_DIR = ../../../output
OUTPUT_TBLS = $(OUTPUT_DIR)/tables/desc_stats_summary.tex \
$(OUTPUT_DIR)/tables/desc_stats_suspicion.tex
