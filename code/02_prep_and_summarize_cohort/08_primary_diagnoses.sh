#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 08_primary_diagnoses.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 08_primary_diagnoses.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/08_primary_diagnoses.R >log/08_primary_diagnoses.log 2>&1
conda deactivate
