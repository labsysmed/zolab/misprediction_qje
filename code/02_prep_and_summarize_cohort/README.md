# Cohort/Analysis Preparation
This directory creates and outputs a variety of summary statistics and data tables for the hold-out set, including the key outcomes and features used for later analyses. The particular goals are outlined below.

- `01_build_risk_factors`: constructs risk factor flags from diagnosis categories; risk factors include atherosclerosis, diabetes, high cholesterol, and hypertension
- `02_clean_demographics`: creates and cleans up demographics variables to be used in analyses
- `03_build_trop_outcomes`: takes troponin lab measures and uses thresholds to construct binary outcomes (used for robustness checks in Appendix 6.1)
- `04_build_cc_df`: takes ED encounter features as an input and modifies labels for easier use later
- `05_build_cohort`: combines raw cohort, outcomes, and model predictions into single cohort table
- `06_get_common_zocats`: report most commonly occuring diagnosis categories
- `07_descriptive_tbl`: builds summary tables for paper (Tables 1 and 2)
- `08_primary_diagnoses`: gets table of primary diagnoses from ED visits
- `09_build_comorbidities` and `10_calc_life_expectancy`: estimates life expectancy from comorbidities and demographic info
- `11_cost_effectiveness`: calculates cost-effectiveness testing (Appendix 2)
- `12_disch_dx`: categorizes ED encounters based on if patients were admitted or discharged, and whether or not patients were diagnosed with a "symptom" (as opposed to a definite disease)
- `13_build_longterm_outcomes`: construct flags for MACE and death 1-2 years post-visit (used in natural experiment, Section 4.4)
- `14_build_subsample_flags`: flags patients who are not (1) admitted with symptom, (2) given a troponin test on the day of their visit, or (3) given an ECG (i.e. no indication that the physician believed they were high-risk) (used in building Figure 3)
