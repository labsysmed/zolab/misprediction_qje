# ------------------------------------------------------------------------------
# Makes all targets for 02_prep_and_summarize_cohort directory
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=10000]" make
# ------------------------------------------------------------------------------

# Config -----------------------------------------------------------------------
.ONESHELL:
include config.mk

# Locals -----------------------------------------------------------------------
SPLIT = random
ifeq ($(SPLIT), overnight)
OVERNIGHT_LAB = _overnight
endif

# Recipes ----------------------------------------------------------------------
## all		: Constructs all targets for 02_prep_and_summarize_cohort
.PHONY: all
all: chief_complaints common_zcs descriptive_tb primary_dx cost_eff disch_dx \
longterm_outcomes subsample_flags output

## risk_factors		: Constructs risk factors (diabetes, hypertension, etc)
.PHONY: risk_factors
risk_factors: $(RISK_FACTORS)
$(RISK_FACTORS): 01_build_risk_factors.sh scripts/01_build_risk_factors.R \
$(DIRS)
	bash $<

## demographics		: Constructs cleaned demographics
.PHONY: demographics
demographics: $(DEMOGRAPHICS)
$(DEMOGRAPHICS): 02_clean_demographics.sh scripts/02_clean_demographics.R \
$(DIRS)
	bash $<

## troponin		: Constructs troponin outcomes for various time horizons
.PHONY: troponin
troponin: $(TROPONIN)
$(TROPONIN) &: 03_build_trop_outcomes.sh scripts/03_build_trop_outcomes.R $(DIRS)
	bash $<

## chief_complaints	: Cleans chief complaints data for all encounters
.PHONY: chief_complaints
chief_complaints: $(CHIEF_COMPLAINTS)
$(CHIEF_COMPLAINTS): 04_build_cc_df.sh scripts/04_build_cc_df.R $(DIRS)
	bash $<

## cohort			: Constructs cohorts (full and test) for analysis
.PHONY: cohort
cohort: $(COHORT)
$(COHORT): 05_build_cohort.sh scripts/05_build_cohort.R $(DIRS) \
$(AMI_ENCOUNTERS) $(RISK_PREDICTIONS)
	bash $< $(SPLIT)

## common_zcs		: Builds CSV with frequency of zocats
.PHONY: common_zcs
common_zcs: $(COMMON_ZCS)
$(COMMON_ZCS): 06_get_common_zocats.sh scripts/06_get_common_zocats.R $(DIRS)
	bash $<

## descriptive_tb		: Builds latex tables with summary stats
.PHONY: descriptive_tb
descriptive_tb: $(DESCRIPTIVE_TB)
$(DESCRIPTIVE_TB): 07_descriptive_tbl.sh scripts/07_descriptive_tbl.R \
$(DIRS) $(RISK_FACTORS) $(DEMOGRAPHICS) $(TROPONIN) $(COHORT) \
../../../lib/summary_tbl_labels.csv
	bash $<

## primary_dx		: Constructs primary diagnosis data
.PHONY: primary_dx
primary_dx: $(PRIMARY_DX)
$(PRIMARY_DX): 08_primary_diagnoses.sh scripts/08_primary_diagnoses.R $(DIRS)
	bash $<

## comorbidities		: Constructs patient comorbidities used in LE calculations
.PHONY: comorbidities
comorbidities: $(COMORBIDITIES)
$(COMORBIDITIES): 09_build_comorbidities.sh scripts/09_build_comorbidities.R \
$(DIRS)
	bash $<

## life_exp		: Constructs life expectancy estimates for all patient-visits
.PHONY: life_exp
life_exp: $(LIFE_EXP)
$(LIFE_EXP): 10_calc_life_expectancy.sh scripts/10_calc_life_expectancy.R \
$(COMORBIDITIES)
	bash $<

## cost_eff		: Calculates cost/life-year for patient-visits
.PHONY: cost_eff
cost_eff: $(COST_EFF)
$(COST_EFF): 11_cost_effectiveness.sh scripts/11_cost_effectiveness.R \
$(LIFE_EXP)
	bash $<

## disch_dx		: Builds primary dx/discharge flags
.PHONY: disch_dx
disch_dx: $(DISCH_DX)
$(DISCH_DX): 12_disch_dx.sh scripts/12_disch_dx.R $(COHORT) $(PRIMARY_DX)
	bash $<

## longterm_outcomes	: Builds longterm adverse outcome variables
.PHONY: longterm_outcomes
longterm_outcomes: $(LONGTERM_OUTCOMES)
$(LONGTERM_OUTCOMES): 13_build_longterm_outcomes.sh \
scripts/13_build_longterm_outcomes.R $(COHORT) $(LIFE_EXP) $(TROPONIN) \
$(DEMOGRAPHICS)
	bash $<

## subsample_flags	: Builds flag vars for subsamples used in analysis
.PHONY: subsample_flags
subsample_flags: $(SUBSAMPLE_FLAGS)
$(SUBSAMPLE_FLAGS): 14_build_subsample_flags.sh \
scripts/14_build_subsample_flags.R $(COHORT) $(DISCH_DX)
	bash $<

## output			: Copies tables used in paper to `output` directory
.PHONY: output
output: $(OUTPUT_TBLS)
$(OUTPUT_DIR)/tables/%.tex : temp/%.tex
	cp $< $@

# Helper Recipes ---------------------------------------------------------------
## dirs			: Creates temp and log directories
.PHONY: dirs
dirs: $(DIRS)
%/.f :
	mkdir -p $(dir $@)
	touch $@

## clean			: Clears temp file folder and log files.
.PHONY: clean
clean: $(DIRS)
	rm -rf temp/*
	rm -f log/*
	rm -rf /data/zolab/stressed_ensemble/data/analysis/*

## style			: Styles R scripts according to tidyverse guidelines
.PHONY: style
style:
	source ~/anaconda3/etc/profile.d/conda.sh
	conda activate stressr
	R --vanilla --silent -e "styler::style_dir(path = 'scripts', filetype = c('R', 'Rmd', 'Rprofile'))"

## help			: Prints available .PHONY targets with description
.PHONY : help
help : Makefile
	@sed -n 's/^##//p' $<
